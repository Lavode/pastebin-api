# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# API documentation: http://pastebin.com/api

import urllib.parse
import urllib.request
import re
import xml.etree.ElementTree
import logging

class APIManager(object):
    ValidPrivacySettings = \
        (
        0, # Public
        1, # Unlisted
        2  # Private
        )

    ValidExpirySettings = \
        (
        "N",    # Never
        "10M",  # 10 minutes
        "1H",   # 1 hour
        "1D",   # 1 day
        "1M"    # 1 month
        )

    ValidFormattingSettings = \
        (
        '4cs',              # 4CS
        '6502acme',         # 6502 ACME Cross Assembler
        '6502kickass',      # 6502 Kick Assembler
        '6502tasm',         # 6502 TASM/64TASS
        'abap',             # ABAP
        'actionscript',     # ActionScript
        'actionscript3',    # ActionScript 3
        'ada',              # Ada
        'algol68',          # ALGOL 68
        'apache',           # Apache Log
        'applescript',      # AppleScript
        'apt_sources',      # APT Sources
        'asm',              # ASM (NASM)
        'asp',              # ASP
        'autoconf',         # autoconf
        'autohotkey',       # Autohotkey
        'autoit',           # AutoIt
        'avisynth',         # Avisynth
        'awk',              # Awk
        'bascomavr',        # BASCOM AVR
        'bash',             # Bash
        'basic4gl',         # Basic4GL
        'bibtex',           # BibTeX
        'blitzbasic',       # Blitz Basic
        'bnf',              # BNF
        'boo',              # BOO
        'bf',               # BrainFuck
        'c',                # C
        'c_mac',            # C for Macs
        'cil',              # C Intermediate Language
        'csharp',           # C#
        'cpp',              # C++
        'cpp-qt',           # C++ (with QT extensions)
        'c_loadrunner',     # C: Loadrunner
        'caddcl',           # CAD DCL
        'cadlisp',          # CAD Lisp
        'cfdg',             # CFDG
        'chaiscript',       # ChaiScript
        'clojure',          # Clojure
        'klonec',           # Clone C
        'klonecpp',         # Clone C++
        'cmake',            # CMake
        'cobol',            # COBOL
        'coffeescript',     # CoffeeScript
        'cfm',              # ColdFusion
        'css',              # CSS
        'cuesheet',         # Cuesheet
        'd',                # D
        'dcs',              # DCS
        'delphi',           # Delphi
        'oxygene',          # Delphi Prism (Oxygene)
        'diff',             # Diff
        'div',              # DIV
        'dos',              # DOS
        'dot',              # DOT
        'e',                # E
        'ecmascript',       # ECMAScript
        'eiffel',           # Eiffel
        'email',            # Email
        'epc',              # EPC
        'erlang',           # Erlang
        'fsharp',           # F#
        'falcon',           # Falcon
        'fo',               # FO Language
        'f1',               # Formula One
        'fortran',          # Fortran
        'freebasic',        # FreeBasic
        'freeswitch',       # FreeSWITCH
        'gambas',           # GAMBAS
        'gml',              # Game Maker
        'gdb',              # GDB
        'genero',           # Genero
        'genie',            # Genie
        'gettext',          # GetText
        'go',               # Go
        'groovy',           # Groovy
        'gwbasic',          # GwBasic
        'haskell',          # Haskell
        'hicest',           # HicEst
        'hq9plus',          # HQ9 Plus
        'html4strict',      # HTML
        'html5',            # HTML 5
        'icon',             # Icon
        'idl',              # IDL
        'ini',              # INI file
        'inno',             # Inno Script
        'intercal',         # INTERCAL
        'io',               # IO
        'j',                # J
        'java',             # Java
        'java5',            # Java 5
        'javascript',       # JavaScript
        'jquery',           # jQuery
        'kixtart',          # KiXtart
        'latex',            # Latex
        'lb',               # Liberty BASIC
        'lsl2',             # Linden Scripting
        'lisp',             # Lisp
        'llvm',             # LLVM
        'locobasic',        # Loco Basic
        'logtalk',          # Logtalk
        'lolcode',          # LOL Code
        'lotusformulas',    # Lotus Formulas
        'lotusscript',      # Lotus Script
        'lscript',          # LScript
        'lua',              # Lua
        'm68k',             # M68000 Assembler
        'magiksf',          # MagikSF
        'make',             # Make
        'mapbasic',         # MapBasic
        'matlab',           # MatLab
        'mirc',             # mIRC
        'mmix',             # MIX Assembler
        'modula2',          # Modula 2
        'modula3',          # Modula 3
        '68000devpac',      # Motorola 68000 HiSoft Dev
        'mpasm',            # MPASM
        'mxml',             # MXML
        'mysql',            # MySQL
        'newlisp',          # newLISP
        'text',             # None
        'nsis',             # NullSoft Installer
        'oberon2',          # Oberon 2
        'objeck',           # Objeck Programming Langua
        'objc',             # Objective C
        'ocaml-brief',      # OCalm Brief
        'ocaml',            # OCaml
        'pf',               # OpenBSD PACKET FILTER
        'glsl',             # OpenGL Shading
        'oobas',            # Openoffice BASIC
        'oracle11',         # Oracle 11
        'oracle8',          # Oracle 8
        'oz',               # Oz
        'pascal',           # Pascal
        'pawn',             # PAWN
        'pcre',             # PCRE
        'per',              # Per
        'perl',             # Perl
        'perl6',            # Perl 6
        'php',              # PHP
        'php-brief',        # PHP Brief
        'pic16',            # Pic 16
        'pike',             # Pike
        'pixelbender',      # Pixel Bender
        'plsql',            # PL/SQL
        'postgresql',       # PostgreSQL
        'povray',           # POV-Ray
        'powershell',       # Power Shell
        'powerbuilder',     # PowerBuilder
        'proftpd',          # ProFTPd
        'progress',         # Progress
        'prolog',           # Prolog
        'properties',       # Properties
        'providex',         # ProvideX
        'purebasic',        # PureBasic
        'pycon',            # PyCon
        'python',           # Python
        'q',                # q/kdb+
        'qbasic',           # QBasic
        'rsplus',           # R
        'rails',            # Rails
        'rebol',            # REBOL
        'reg',              # REG
        'robots',           # Robots
        'rpmspec',          # RPM Spec
        'ruby',             # Ruby
        'gnuplot',          # Ruby Gnuplot
        'sas',              # SAS
        'scala',            # Scala
        'scheme',           # Scheme
        'scilab',           # Scilab
        'sdlbasic',         # SdlBasic
        'smalltalk',        # Smalltalk
        'smarty',           # Smarty
        'sql',              # SQL
        'systemverilog',    # SystemVerilog
        'tsql',             # T-SQL
        'tcl',              # TCL
        'teraterm',         # Tera Term
        'thinbasic',        # thinBasic
        'typoscript',       # TypoScript
        'unicon',           # Unicon
        'uscript',          # UnrealScript
        'vala',             # Vala
        'vbnet',            # VB.NET
        'verilog',          # VeriLog
        'vhdl',             # VHDL
        'vim',              # VIM
        'visualprolog',     # Visual Pro Log
        'vb',               # VisualBasic
        'visualfoxpro',     # VisualFoxPro
        'whitespace',       # WhiteSpace
        'whois',            # WHOIS
        'winbatch',         # Winbatch
        'xbasic',           # XBasic
        'xml',              # XML
        'xorg_conf',        # Xorg Config
        'xpp',              # XPP
        'yaml',             # YAML
        'z80',              # Z80 Assembler
        'zxbasic',          # ZXBasic
        )

    APIManagementURL = "http://pastebin.com/api/api_post.php"
    APILoginURL = "http://pastebin.com/api/api_login.php"
    APIRawURL = "http://pastebin.com/raw.php?i={0}"

    APIKey_Reegx = "[0-f]{32}"

    def __init__(self):
        self._APIKey = None
        self._UserKey = None

        self.User = None
        self.Password = None

        self.cAPIKey_Regex = re.compile(APIManager.APIKey_Reegx)

    def _get_api_key(self):
        return self._APIKey

    def _set_api_key(self, value):
        if not self.cAPIKey_Regex.match(value):
            logging.warning("API key might be invalid: " + value)
        self._APIKey = str(value)

    APIKey = property(_get_api_key, _set_api_key)

    def _get_user_key(self):
        return self._UserKey

    def _set_user_key(self, value):
        if not self.cAPIKey_Regex.match(value):
            logging.warning("User key might be invalid: " + value)
        self._UserKey = str(value)

    UserKey = property(_get_user_key, _set_user_key)


    def create_paste(self, paste, name="Untitled", paste_format="text", privacy=0, expiry_date="N", use_authentication=False, userkey=None):
        """Creates a new paste.

        Parameters:
        paste -- The text which to paste.
        name="Untitled" -- The name of the paste.
        paste_format="text" -- The text highlighter to use.
        privacy=0 -- 0=Public, 1=Unlisted, 2=Private.
        expiry_date="N" -- "N"=Never, "10M"=10 minutes, "1H"=1 hour, "1D"=1 day, "1M"=1 month.
        use_authentication=False -- If True it'll create the paste under your account, else as a guest.
        userkey=None -- If use_authentication is set to True this setting can be used to override the stored userkey.

        Returns:
        str -- The URL to the newly created paste.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if an invalid privacy, formatting or expiry setting was supplied.

        """
        if userkey is None:
            userkey = self._UserKey

        if privacy not in APIManager.ValidPrivacySettings:
            raise ValueError("Invalid paste privacy setting supplied.")
        if paste_format not in APIManager.ValidFormattingSettings:
            raise ValueError("Invalid paste formatting settings supplied.")
        if expiry_date not in APIManager.ValidExpirySettings:
            raise ValueError("Invalid expirty date supplied.")

        args = {"api_dev_key": self._APIKey,
                "api_option": "paste",
                "api_paste_code": paste,
                "api_paste_name": name,
                "api_paste_format": paste_format,
                "api_paste_private": privacy,
                "api_paste_expire_date": expiry_date}
        if use_authentication:
            args["api_user_key"] = userkey

        result = self._do_api_post_request(APIManager.APIManagementURL, args)
        if "Bad API request" in result:
            raise APIError(args, result)
        return result

    def create_unlisted_paste(self, paste, name="Untitled", paste_format="text", expiry_date="N", use_authentication=False, userkey=None):
        """Creates an unlisted paste.

        Parameters:
        paste -- The text which to paste.
        name="Untitled" -- The name of the paste.
        paste_format="text" -- The text highlighter to use.
        expiry_date="N" -- "N"=Never, "10M"=10 minutes, "1H"=1 hour, "1D"=1 day, "1M"=1 month.
        use_authentication=False -- If True it'll create the paste under your account, else as a guest.
        userkey=None -- If use_authentication is set to True this setting can be used to override the stored userkey.

        Returns:
        str -- The URL to the newly created paste.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if an invalid privacy, formatting or expiry setting was supplied.

        """
        self.create_paste(paste, name, paste_format, 1, expiry_date, use_authentication, userkey)

    def create_private_paste(self, paste, name="Untitled", paste_format="text", expiry_date="N", userkey=None):
        """Creates a private paste.

        Parameters:
        paste -- The text which to paste.
        name="Untitled" -- The name of the paste.
        paste_format="text" -- The text highlighter to use.
        expiry_date="N" -- "N"=Never, "10M"=10 minutes, "1H"=1 hour, "1D"=1 day, "1M"=1 month.
        use_authentication=False -- If True it'll create the paste under your account, else as a guest.
        userkey=None -- If use_authentication is set to True this setting can be used to override the stored userkey.

        Returns:
        str -- The URL to the newly created paste.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """
        self.create_paste(paste, name, paste_format, 2, expiry_date, userkey)


    def delete_paste(self, paste_id, userkey=None):
        """Deletes an existing paste. Must be logged in to do that!

        Parameters:
        paste_id -- The ID of the paste which to delete.
        userkey=None -- This setting can be used to override the stored userkey.

        Returns:
        bool -- True if the paste was removed successfully..

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if an invalid privacy, formatting or expiry setting was supplied.

        """
        if userkey is None:
            userkey = self._UserKey

        args = {"api_dev_key": self._APIKey,
                "api_option": "delete",
                "api_user_key": userkey,
                "api_paste_key": paste_id}

        result = self._do_api_post_request(APIManager.APIManagementURL, args)
        if "Bad API request" in result:
            raise APIError(args, result)
        elif result == "Paste removed":
            return True


    def list_user_pastes(self, userkey=None, limit=50):
        """Lists a user's pastes.

        Parameters:
        userkey=None -- If set to anything but None this will make the query use the specified userkey instead of the stored one.

        Returns:
        list -- [{"URL": str, "Formatting_short": str, "Expiry_date": int, "Key": str, "Title": str, "Date": int, "Privacy": int, "Hits": int, "Formatting": str, "Size": int}]

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if the limit was too big or too small.

        """
        if limit < 1 or limit > 1000:
            raise ValueError("limit must be between 1 and 1000.")

        if userkey is None:
            userkey = self._UserKey

        args = {"api_dev_key": self._APIKey,
                "api_option": "list",
                "api_user_key": userkey,
                "api_results_limit": limit}

        input = self._do_api_post_request(APIManager.APIManagementURL, args)
        if "Bad API request" in input:
            raise APIError(args, input)

        # For whatever reason there's linebreaks in there.
        input = input.replace("\r\n","")
        # There also is no root element, so there's this ugly workaround.
        input = "<pastes>" + input + "</pastes>"


        result = []

        tree = xml.etree.ElementTree.fromstring(input)
        # Equals "for x in tree.getchildren(), for y in x.getchildren()"
        for paste_entry in tree:
            paste = {"Key": None,
                     "Date": None,
                     "Title": None,
                     "Size": None,
                     "Expiry_date": None,
                     "Privacy": None,
                     "Formatting": None,
                     "Formatting_short": None,
                     "URL": None,
                     "Hits": None}
            for paste_detail in paste_entry:
                if paste_detail.tag == "paste_key":
                    paste["Key"] = paste_detail.text
                elif paste_detail.tag == "paste_date":
                    paste["Date"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_title":
                    paste["Title"] = paste_detail.text
                elif paste_detail.tag == "paste_size":
                    paste["Size"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_expire_date":
                    paste["Expiry_date"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_private":
                    paste["Privacy"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_format_long":
                    paste["Formatting"] = paste_detail.text
                elif paste_detail.tag == "paste_format_short":
                    paste["Formatting_short"] = paste_detail.text
                elif paste_detail.tag == "paste_url":
                    paste["URL"] = paste_detail.text
                elif paste_detail.tag == "paste_hits":
                    paste["Hits"] = int(paste_detail.text)

            # Individual paste entry has been parsed, adding to the list.
            result.append(paste)

        # All has been parsed.
        return result

    def list_trending_pastes(self):
        """Lists trending pastes.

        Parameters:

        Returns:
        list -- [{"URL": str, "Formatting_short": str, "Expiry_date": int, "Key": str, "Title": str, "Date": int, "Privacy": int, "Hits": int, "Formatting": str, "Size": int}]

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if the limit was too big or too small.

        """
        args = {"api_dev_key": self._APIKey,
                "api_option": "trends"}

        input = self._do_api_post_request(APIManager.APIManagementURL, args)
        if "Bad API request" in input:
            raise APIError(args, input)

        # For whatever reason there's linebreaks in there.
        input = input.replace("\r\n","")
        # There also is no root element, so there's this ugly workaround.
        input = "<pastes>" + input + "</pastes>"


        result = []

        tree = xml.etree.ElementTree.fromstring(input)
        # Equals "for x in tree.getchildren(), for y in x.getchildren()"
        for paste_entry in tree:
            paste = {"Key": None,
                     "Date": None,
                     "Title": None,
                     "Size": None,
                     "Expiry_date": None,
                     "Privacy": None,
                     "Formatting": None,
                     "Formatting_short": None,
                     "URL": None,
                     "Hits": None}
            for paste_detail in paste_entry:
                if paste_detail.tag == "paste_key":
                    paste["Key"] = paste_detail.text
                elif paste_detail.tag == "paste_date":
                    paste["Date"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_title":
                    paste["Title"] = paste_detail.text
                elif paste_detail.tag == "paste_size":
                    paste["Size"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_expire_date":
                    paste["Expiry_date"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_private":
                    paste["Privacy"] = int(paste_detail.text)
                elif paste_detail.tag == "paste_format_long":
                    paste["Formatting"] = paste_detail.text
                elif paste_detail.tag == "paste_format_short":
                    paste["Formatting_short"] = paste_detail.text
                elif paste_detail.tag == "paste_url":
                    paste["URL"] = paste_detail.text
                elif paste_detail.tag == "paste_hits":
                    paste["Hits"] = int(paste_detail.text)

            # Individual paste entry has been parsed, adding to the list.
            result.append(paste)

        # All has been parsed.
        return result


    def get_user_info(self, userkey=None):
        """Lists a user's default settings and additional information.

        Parameters:
        userkey=None -- If set to anything but None this will make the query use the specified userkey instead of the stored one.

        Returns:
        dict -- {"Website": str, "isPremiumAccount: bool, "Formatting_short": str, "Expiry_date": str, "User": str, "Privacy": int, "Avatar_URL": str, "Email": str, "Location": str}

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """
        if userkey is None:
            userkey = self._UserKey

        args = {"api_dev_key": self._APIKey,
                "api_option": "userdetails",
                "api_user_key": userkey}

        input = self._do_api_post_request(APIManager.APIManagementURL, args)
        if "Bad API request" in input:
            raise APIError(args, input)

        # For whatever reason there's linebreaks in there.
        input = input.replace("\r\n","")

        result = {"User": None,
                  "Formatting_short": None,
                  "Expiry_date": None,
                  "Avatar_URL": None,
                  "Privacy": None,
                  "Website": None,
                  "Email": None,
                  "Location": None,
                  "isPremiumAccount": None}

        tree = xml.etree.ElementTree.fromstring(input)

        for user_detail in tree:

            if user_detail.tag == "user_name":
                result["User"] = user_detail.text
            elif user_detail.tag == "user_format_short":
                result["Formatting_short"] = user_detail.text
            elif user_detail.tag == "user_expiration":
                result["Expiry_date"] = user_detail.text
            elif user_detail.tag == "user_avatar_url":
                result["Avatar_URL"] = user_detail.text
            elif user_detail.tag == "user_private":
                result["Privacy"] = int(user_detail.text)
            elif user_detail.tag == "user_website":
                result["Website"] = user_detail.text
            elif user_detail.tag == "user_email":
                result["Email"] = user_detail.text
            elif user_detail.tag == "user_location":
                result["Location"] = user_detail.text
            elif user_detail.tag == "user_account_type":
                # "1" -> 1 -> True, "0" -> 0 -> False
                result["isPremiumAccount"] = bool(int(user_detail.text))

        return result

    def get_raw_paste(self, paste_id):
        """Returns a paste's raw text.

        Parameters:
        paste_id -- The ID of the paste which to return the text of.

        Returns:
        str -- The paste's text.

        Exceptions:

        """
        return self._do_api_get_request(APIManager.APIRawURL, paste_id)


    def do_login(self, user=None, password=None):
        """Returns a userkey for the specified username and password. This userkey can be used to retrieve a user's information or pastes, and to create pastes as a specific user.

        Parameters:
        user=None -- If set to anything other than None this will be used instead of the stored username.
        password=None -- If set to anything other than None this will be used instead of the stored password.

        Returns:
        int -- The userkey.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """
        if user is None:
            user = self.User
        if password is None:
            password = self.Password

        args = {"api_dev_key": self._APIKey,
                "api_user_name": user,
                "api_user_password": password}

        result = self._do_api_post_request(APIManager.APILoginURL, args)
        if "Bad API request" in result:
            raise APIError(args, result)
        else:
            return result

    def _do_api_post_request(self, url, args):
        logging.debug("Doing POST request to " + url + " with the following arguments:\n" + str(args))
        args_encoded = urllib.parse.urlencode(args).encode("utf-8")

        request = urllib.request.Request(url, args_encoded)
        response = urllib.request.urlopen(request)

        result =  response.read().decode("utf-8")
        logging.debug("Result: " + result)
        return result

    def _do_api_get_request(self, url, arg):
        logging.debug("Doing GET request to " + url + " with the following argumen: " + str(arg))
        response = urllib.request.urlopen(url.format(arg))

        result = response.read().decode("utf-8")
        logging.debug("Result: " + result)
        return result


class Paste(object):
    def __init__(self):
        self.Paste = None
        self.Title = "untitled"
        self._Format = "text"
        self._Privacy = 0
        self.ExpiryDate = "N"
        self.Key = None
        self.Date = None
        self.Hits = None
        self.Formatting = None
        self.Size = None


    def get_Format(self):
        return self._Format

    def set_Format(self, value):
        if value not in APImanager.ValidFormattingSettings:
            raise ValueError("Invalid formatting setting supplied!")
        self._Format = value

    Format = property(get_Format, set_Format)

    def get_Privacy(self):
        return self._Privacy

    def set_Privacy(self, value):
        if value not in APIManager.ValidPrivacySettings:
            raise ValueError("Invalid privacy setting supplied!")
        self._Privacy = value

    Privacy = property(get_Privacy, set_Privacy)


    def import_from_dictionary(self, d):
        # Todo: Docstrings, Format should be Formatting_short, or I might just change the other one.
        try:
            self.Title = d["Title"]
            self.Format = d["Formatting_short"]
            self.Formatting = d["Formatting"]
            self.Privacy = d["Privacy"]
            self.ExpiryDate = d["Expiry_date"]
            self.Key = d["Key"]
            self.Date = d["Date"]
            self.Hits = d["Hits"]
            self.Size = d["Size"]
        except KeyError:
            logging.warning("\n".join(["Paste/import_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))

class APIError(Exception):
    def __init__(self, query=None, result=None, message=None):
        self.Query = query
        self.Result = result
        self.Message = message


